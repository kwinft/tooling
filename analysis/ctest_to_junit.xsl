<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/Site">
        <xsl:variable name="Name"><xsl:value-of select="@Name"/></xsl:variable>
        <xsl:variable name="Hostname"><xsl:value-of select="@Hostname"/></xsl:variable>
        <xsl:variable name="FailureCount"><xsl:value-of select="count(//Testing/Test[@Status='failed'])"/> </xsl:variable>
        <xsl:variable name="TestCount"><xsl:value-of select="count(//TestList/Test)"/> </xsl:variable>

        <testsuite name="{$Name}" hostname="{$Hostname}" errors="0" failures="{$FailureCount}" tests="{$TestCount}">
            <xsl:apply-templates select="Testing/Test"/>
        </testsuite>
    </xsl:template>

    <xsl:template match="/Site/Testing/Test">
        <xsl:variable name="testcasename"><xsl:value-of select= "Name"/></xsl:variable>
        <xsl:variable name="testclassname"><xsl:value-of select= "substring(Path,2)"/></xsl:variable>
        <xsl:variable name="exectime">
            <xsl:for-each select="Results/NamedMeasurement">
                <xsl:if test="@name = 'Execution Time'">
                    <xsl:value-of select="format-number(.,'0.00000')"/>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>

        <testcase name="{$testcasename}" classname="{$testclassname}" time="{$exectime}">
            <xsl:if test="@Status = 'failed'">
                <xsl:variable name="failtype">
                    <xsl:for-each select="Results/NamedMeasurement">
                        <xsl:if test="@name = 'Exit Code'">
                            <xsl:value-of select="normalize-space(.)"/>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="failcode">
                    <xsl:for-each select="Results/NamedMeasurement">
                        <xsl:if test="@name = 'Exit Value'">
                            <xsl:value-of select="format-number(., 0)"/>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:variable>
                <failure message="{$failtype} ({$failcode})"><xsl:value-of select="Results/Measurement/Value/text()" /></failure>
            </xsl:if>

            <xsl:if test="@Status = 'notrun'">
                <skipped><xsl:value-of select="Results/Measurement/Value/text()" /></skipped>
            </xsl:if>
        </testcase>
    </xsl:template>
</xsl:stylesheet>
