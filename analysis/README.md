# Analysis Tooling
## Tests
An XSL template file is provided with which CTest results
can be converted to the JUnit format.

## Static
### Clang
* clang-format wrapper: [Credit][clang-format-wrapper], [MIT][mit-license].
* clang-tidy wrapper: [Credit][clang-tidy-wrapper], [LLVM Release License][llvm-license].

These scripts can be used locally too, what is simplified through the `clang-*.sh` scripts.

> :information_source: For clang-tidy the project must have been compiled with the
> `-DCMAKE_EXPORT_COMPILE_COMMANDS=ON` CMake option.

## CI integration
GitLab job templates are provided in the `gitlab-ci` directory:

* For running CTest with and without coverage reporting.
* Retrieving current coverage with and without tests being run in the pipeline.
* For running clang-format and clang-tidy.

The Clang template jobs assume the repo contains a directory `tooling` with `clang-*.sh` scripts to run.
If such a script does not exist the default version from here is copied over and run instead.

[clang-format-wrapper]: https://github.com/Sarcasm/run-clang-format
[clang-tidy-wrapper]: https://github.com/Sarcasm/irony-mode/tree/master/server/build-aux/run-clang-tidy
[mit-license]: https://opensource.org/licenses/MIT
[llvm-license]: https://releases.llvm.org/2.8/LICENSE.TXT
