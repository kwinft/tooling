# Common tooling for KWinFT projects

This repo contains scripts and other tools
to develop and maintain the KWinFT projects.

As of now the following tooling is provided:
* analysis: dynamic (tests) and static code analysis
* docs: documentation support
* i18n: tools for internationalization
